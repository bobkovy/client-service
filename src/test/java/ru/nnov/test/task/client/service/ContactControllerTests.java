package ru.nnov.test.task.client.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import ru.nnov.test.task.client.service.entity.Contact;
import ru.nnov.test.task.client.service.exceptions.ContactNotFoundException;
import ru.nnov.test.task.client.service.repository.ContactRepository;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ContactControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ContactRepository contactRepository;

    @Test
    void getContactTest() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Contact> response = restTemplate.exchange("/contacts/aca66f08-1a4c-40a7-88ee-69dcc0d12316",
                HttpMethod.GET,
                entity, new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var contactFromResponse = response.getBody();
        assertNotNull(contactFromResponse);
        assertNotNull(contactFromResponse.getId());
        assertEquals("Cain Cook", contactFromResponse.getContactName());
        assertEquals("aca66f08-1a4c-40a7-88ee-69dcc0d12316", contactFromResponse.getId().toString());
    }

    @Test
    void getContact404Test() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Contact> response = restTemplate.exchange("/contacts/00000000-1a4c-40a7-88ee-69dcc0d12316",
                HttpMethod.GET,
                entity, new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void createContactTest() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Contact> response = restTemplate.exchange("/contacts?name=contact-name",
                HttpMethod.POST,
                entity, new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        var contactFromResponse = response.getBody();
        assertNotNull(contactFromResponse);
        assertNotNull(contactFromResponse.getId());
        assertEquals("contact-name", contactFromResponse.getContactName());
        var contactFromDb = contactRepository.findById(contactFromResponse.getId()).orElseThrow(() -> new ContactNotFoundException(contactFromResponse.getId()));
        assertEquals("contact-name", contactFromDb.getContactName());
    }

    @Test
    void createContact400Test() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(
                "/contacts?name=0123456789012345678901234567890123456789",
                HttpMethod.POST,
                entity, new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void updateContactTest() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Contact> response = restTemplate.exchange(
                "/contacts/6476deb5-99eb-448b-88bd-ee24003072a4?name=Emilee Short2",
                HttpMethod.PATCH,
                entity,
                new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var contactFromResponse = response.getBody();
        assertNotNull(contactFromResponse);
        assertNotNull(contactFromResponse.getId());
        assertEquals("Emilee Short2", contactFromResponse.getContactName());
        var contactFromDb = contactRepository.findById(contactFromResponse.getId()).orElseThrow(() -> new ContactNotFoundException(contactFromResponse.getId()));
        assertEquals("Emilee Short2", contactFromDb.getContactName());
    }

    @Test
    void updateContact400Test() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Contact> response = restTemplate.exchange(
                "/contacts/6476deb5-99eb-448b-88bd-ee24003072a4?name=0123456789012345678901234567890123456789",
                HttpMethod.PATCH,
                entity,
                new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void deleteContactTest() {
        ResponseEntity<Contact> response = restTemplate.exchange("/contacts/c54db8b0-63a0-4257-8aa2-2b7a8689e518",
                HttpMethod.DELETE,
                null, new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertFalse(contactRepository.findById(UUID.fromString("c54db8b0-63a0-4257-8aa2-2b7a8689e518")).isPresent());
    }

}
