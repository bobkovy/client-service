package ru.nnov.test.task.client.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import ru.nnov.test.task.client.service.entity.Contact;
import ru.nnov.test.task.client.service.exceptions.ApplicationNotFoundException;
import ru.nnov.test.task.client.service.repository.ApplicationRepository;

import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApplicationControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Test
    void getApplicationTest() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange("/applications/4d15c839-2e72-4854-9290-dca2a8100004",
                HttpMethod.GET,
                entity, new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(
                "{\"DT_CREATED\":\"2019-11-19T04:07:35\"," +
                        "\"PRODUCT_NAME\":\"PRODUCT_7\"," +
                        "\"APPLICATION_ID\":\"4d15c839-2e72-4854-9290-dca2a8100004\"," +
                        "\"CONTACT_ID\":\"6476deb5-99eb-448b-88bd-ee2400300002\"}",
                response.getBody());
    }

    @Test
    void getApplication404Test() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange("/applications/00000000-1a4c-40a7-88ee-69dcc0d12316",
                HttpMethod.GET,
                entity, new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void createApplicationTest() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Map<String, String>> response = restTemplate.exchange(
                "/applications?contactUUID=aca66f08-1a4c-40a7-88ee-69dcc0d12316&productName=PRODUCT_Z",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        var applicationFromResponse = response.getBody();
        assertNotNull(applicationFromResponse);
        assertNotNull(applicationFromResponse.get("APPLICATION_ID"));
        assertNotNull(applicationFromResponse.get("DT_CREATED"));
        assertEquals("PRODUCT_Z", applicationFromResponse.get("PRODUCT_NAME"));
        assertEquals("aca66f08-1a4c-40a7-88ee-69dcc0d12316", applicationFromResponse.get("CONTACT_ID"));
    }


    @Test
    void createApplicationProductNameNull400Test() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(
                "/applications?contactUUID=aca66f08-1a4c-40a7-88ee-69dcc0d12316&productName=",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void createApplicationContactUUIDNull400Test() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange("/applications?contactUUID=&productName=PRODUCT_Z",
                HttpMethod.POST,
                entity, new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void updateApplicationTest() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Map<String, String>> response = restTemplate.exchange(
                "/applications/9f3fa689-d81f-45ab-88ef-a616f0e3e6ee?contactUUID=6476deb5-99eb-448b-88bd-ee24003072a4&productName=PRODUCT_Y",
                HttpMethod.PATCH,
                entity,
                new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var applicationFromResponse = response.getBody();
        assertNotNull(applicationFromResponse);
        assertEquals("9f3fa689-d81f-45ab-88ef-a616f0e3e6ee", applicationFromResponse.get("APPLICATION_ID"));
        //assertEquals("2019-11-17T00:00:00", applicationFromResponse.get("DT_CREATED"));
        assertEquals("PRODUCT_Y", applicationFromResponse.get("PRODUCT_NAME"));
        assertEquals("6476deb5-99eb-448b-88bd-ee24003072a4", applicationFromResponse.get("CONTACT_ID"));
        var updatedApplicationUUID = UUID.fromString("9f3fa689-d81f-45ab-88ef-a616f0e3e6ee");
        var updatedApplication = applicationRepository
                .findById(updatedApplicationUUID)
                .orElseThrow(() -> new ApplicationNotFoundException(updatedApplicationUUID));
        assertEquals("PRODUCT_Y", updatedApplication.getProductName());
        assertEquals("6476deb5-99eb-448b-88bd-ee24003072a4", updatedApplication.getContact().getId().toString());
    }


    @Test
    void deleteContactTest() {
        ResponseEntity<Contact> response = restTemplate.exchange("/applications/d0357de2-e661-4323-ab44-d359e7c058ec",
                HttpMethod.DELETE,
                null, new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertFalse(applicationRepository.findById(UUID.fromString("d0357de2-e661-4323-ab44-d359e7c058ec"))
                .isPresent());
    }

    @Test
    void deleteContact400Test() {
        ResponseEntity<Contact> response = restTemplate.exchange("/applications/c54db8b0-63a0-4257-8aa2-2b7a8689e518",
                HttpMethod.DELETE,
                null, new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertFalse(applicationRepository.findById(UUID.fromString("c54db8b0-63a0-4257-8aa2-2b7a8689e518"))
                .isPresent());
    }

    @Test
    void findLastContactTest() {
        ResponseEntity<Map<String, String>> response = restTemplate.exchange(
                "/applications/latest?contactUUID=6476deb5-99eb-448b-88bd-ee2400300001",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var applicationFromResponse = response.getBody();
        assertNotNull(applicationFromResponse);
        assertEquals("4d15c839-2e72-4854-9290-dca2a8100003", applicationFromResponse.get("APPLICATION_ID"));
        assertEquals("2019-11-19T04:07:00", applicationFromResponse.get("DT_CREATED"));
        assertEquals("PRODUCT_3", applicationFromResponse.get("PRODUCT_NAME"));
        assertEquals("6476deb5-99eb-448b-88bd-ee2400300001", applicationFromResponse.get("CONTACT_ID"));
    }
}
