package ru.nnov.test.task.client.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import wiremock.org.apache.commons.io.FileUtils;

import java.io.File;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GenerateSwaggerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void generateSwaggerTest() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> swaggerDoc = restTemplate.exchange("/v2/api-docs",
                HttpMethod.GET,
                entity, new ParameterizedTypeReference<>() {
                });
        FileUtils.writeStringToFile(new File("target/swagger.json"), swaggerDoc.getBody());
    }
}