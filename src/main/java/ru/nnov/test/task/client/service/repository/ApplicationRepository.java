package ru.nnov.test.task.client.service.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nnov.test.task.client.service.entity.Application;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ApplicationRepository extends CrudRepository<Application, UUID> {

    Optional<Application> findFirstByContactIdOrderByCreatedDateDesc(UUID contactUUID);

    List<Application> findByContactIdOrderByCreatedDateDesc(UUID contactUUID);
}
