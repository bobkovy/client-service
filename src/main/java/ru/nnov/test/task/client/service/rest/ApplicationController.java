package ru.nnov.test.task.client.service.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import ru.nnov.test.task.client.service.entity.Application;
import ru.nnov.test.task.client.service.exceptions.ApplicationNotFoundException;
import ru.nnov.test.task.client.service.exceptions.ContactNotFoundException;
import ru.nnov.test.task.client.service.repository.ApplicationRepository;
import ru.nnov.test.task.client.service.repository.ContactRepository;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@RequestMapping("/applications")
@Validated
@Transactional
public class ApplicationController {
    private ApplicationRepository applicationRepository;
    private ContactRepository contactRepository;

    public ApplicationController(ApplicationRepository applicationRepository,
                                 ContactRepository contactRepository) {
        this.applicationRepository = applicationRepository;
        this.contactRepository = contactRepository;
    }

    @ApiOperation(value = "Create new application")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Returns new application"),
            @ApiResponse(code = 400, message = "Product name or contact unique identifier have wrong format ( 0 < length < 36)")})
    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Application> createApplication(@ApiParam(example = "Easy money", required = true) @RequestParam @NotBlank @Size(min = 1, max = 36) String productName,
                                                         @ApiParam(example = "6476deb5-99eb-448b-88bd-ee24003072a4", required = true) @RequestParam UUID contactUUID) {
        var application = Application.builder()
                .productName(productName)
                .contact(contactRepository.findById(contactUUID)
                        .orElseThrow(() -> new ContactNotFoundException(contactUUID)))
                .build();
        applicationRepository.save(application);
        return ResponseEntity.created(MvcUriComponentsBuilder.fromMethodCall(on(getClass()).getApplication(application.getId()))
                .build()
                .toUri())
                .body(application);
    }

    @PatchMapping(path = "/{applicationUUID}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    @ApiOperation(value = "Update application by unique identifier")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns updated application"),
            @ApiResponse(code = 404, message = "Application not found"),
            @ApiResponse(code = 400, message = "Product name or contact unique identifier have wrong format ( 0 < length < 36)")})
    public Application patchApplication(@PathVariable UUID applicationUUID,
                                        @ApiParam(example = "Crazy money") @RequestParam(required = false) @Size(max = 36) String productName,
                                        @ApiParam(example = "c54db8b0-63a0-4257-8aa2-2b7a8689e518") @RequestParam(required = false) UUID contactUUID) {
        var application = getApplication(applicationUUID);

        Optional.ofNullable(productName).filter(name -> !name.isBlank()).ifPresent(application::setProductName);
        Optional.ofNullable(contactUUID)
                .map(uuid -> contactRepository.findById(uuid).orElseThrow(() -> new ContactNotFoundException(uuid)))
                .ifPresent(application::setContact);
        return application;
    }

    @GetMapping(path = "/{applicationUUID}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    @ApiOperation(value = "Find application by unique identifier")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns application associated with unique identifier"),
            @ApiResponse(code = 404, message = "Application not found")})
    public Application getApplication(@ApiParam(example = "9f3fa689-d81f-45ab-88ef-a616f0e3e6ee", required = true) @PathVariable UUID applicationUUID) {
        return applicationRepository.findById(applicationUUID)
                .orElseThrow(() -> new ApplicationNotFoundException(applicationUUID));
    }

    @DeleteMapping(path = "/{applicationUUID}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(OK)
    @ApiOperation(value = "Delete application by unique identifier")
    @ApiResponses({
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 404, message = "Application not found")})
    public void deleteApplication(@ApiParam(example = "9f3fa689-d81f-45ab-88ef-a616f0e3e6ee", required = true) @PathVariable UUID applicationUUID) {
        applicationRepository.deleteById(applicationUUID);
    }

    @GetMapping(path = "/latest", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    @ApiOperation(value = "Find latest created application associated with contact")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns last created application associated with contact"),
            @ApiResponse(code = 404, message = "No applications for contact")})
    public Application getLatestApplication(@ApiParam(example = "aca66f08-1a4c-40a7-88ee-69dcc0d12316", required = true) @RequestParam UUID contactUUID) {
        return applicationRepository.findFirstByContactIdOrderByCreatedDateDesc(contactUUID)
                .orElseThrow(() -> new ApplicationNotFoundException("No applications for contact (" + contactUUID + ")"));
    }

    @GetMapping(path = "/all", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    @ApiOperation(value = "Find all application associated with contact")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns all application associated with contact")})
    public List<Application> getAllApplication(@ApiParam(example = "aca66f08-1a4c-40a7-88ee-69dcc0d12316", required = true) @RequestParam UUID contactUUID) {
        return applicationRepository.findByContactIdOrderByCreatedDateDesc(contactUUID);
    }
}
