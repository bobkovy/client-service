package ru.nnov.test.task.client.service.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import ru.nnov.test.task.client.service.entity.Contact;
import ru.nnov.test.task.client.service.exceptions.ContactNotFoundException;
import ru.nnov.test.task.client.service.repository.ContactRepository;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@Transactional
@RequestMapping("/contacts")
@Validated
public class ContactController {
    private ContactRepository contactRepository;

    public ContactController(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @ApiOperation(value = "Create new contact")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Returns new contact"),
            @ApiResponse(code = 400, message = "Name parameter has wrong format ( 0 < length < 36)")})
    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Contact> createContact(
            @ApiParam(example = "Sidra Alston", required = true) @RequestParam @NotBlank @Size(min = 1, max = 36) String name) {
        var contact = Contact.builder()
                .contactName(name)
                .build();

        contactRepository.save(contact);

        return ResponseEntity.created(MvcUriComponentsBuilder.fromMethodCall(on(getClass()).getContact(contact.getId()))
                .build()
                .toUri())
                .body(contact);
    }

    @GetMapping(path = "/{contactUUID}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    @ApiOperation(value = "Find contact by unique identifier")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns contact associated with unique identifier"),
            @ApiResponse(code = 404, message = "Contact not found")})
    public Contact getContact(@ApiParam(example = "aca66f08-1a4c-40a7-88ee-69dcc0d12316",
            required = true) @PathVariable UUID contactUUID) {
        return contactRepository.findById(contactUUID)
                .orElseThrow(() -> new ContactNotFoundException(contactUUID));
    }

    @PatchMapping(path = "/{contactUUID}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    @ApiOperation(value = "Find contact by unique identifier and update his name")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns updated contact"),
            @ApiResponse(code = 404, message = "Contact not found"),
            @ApiResponse(code = 400, message = "Unique identifier  or name have wrong format ( 0 < length < 36)")})
    public Contact updateContact(
            @ApiParam(example = "aca66f08-1a4c-40a7-88ee-69dcc0d12316", required = true) @PathVariable UUID contactUUID,
            @ApiParam(example = "Yaseen Newton", required = true)
            @RequestParam @NotBlank @Size(min = 1, max = 36) String name) {
        var contact = contactRepository.findById(contactUUID)
                .orElseThrow(() -> new ContactNotFoundException(contactUUID));
        contact.setContactName(name);
        return contact;
    }

    @DeleteMapping(path = "/{contactUUID}")
    @ResponseStatus(code = OK)
    @ApiOperation(value = "Delete contact by unique identifier")
    @ApiResponses({
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 404, message = "Contact not found")})
    public void deleteContact(@ApiParam(example = "aca66f08-1a4c-40a7-88ee-69dcc0d12316", required = true) @PathVariable UUID contactUUID) {
        contactRepository.deleteById(contactUUID);
    }

    @ApiOperation(value = "Retrieve all contacts")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns all contacts")})
    @GetMapping(path = "/all", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Iterable<Contact> getAllContacts() {
        return contactRepository.findAll();
    }
}
