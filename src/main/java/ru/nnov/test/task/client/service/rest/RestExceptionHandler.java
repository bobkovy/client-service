package ru.nnov.test.task.client.service.rest;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.nnov.test.task.client.service.exceptions.ApplicationNotFoundException;
import ru.nnov.test.task.client.service.exceptions.ContactNotFoundException;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value
            = {ContactNotFoundException.class, ApplicationNotFoundException.class, EmptyResultDataAccessException.class})
    protected ResponseEntity<Object> handleNotFound(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex,
                new ErrorResponse(ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND,
                request);
    }

    @ExceptionHandler(value
            = {ConstraintViolationException.class, IllegalArgumentException.class})
    protected ResponseEntity<Object> handleConstraintViolation(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex,
                new ErrorResponse(ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    @AllArgsConstructor
    @Getter
    @JsonRootName("Error")
    private static class ErrorResponse {
        private String message;
    }
}
