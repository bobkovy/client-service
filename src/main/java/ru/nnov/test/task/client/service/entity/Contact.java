package ru.nnov.test.task.client.service.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "CONTACT")
@EqualsAndHashCode()
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"contactId", "contactName"})
@ApiModel
public class Contact {
    @Id
    @Column(name = "CONTACT_ID")
    @JsonProperty("CONTACT_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(example = "aca66f08-1a4c-40a7-88ee-69dcc0d12316")
    private UUID id;
    @Column(name = "CONTACT_NAME", length = 36)
    @JsonProperty("CONTACT_NAME")
    @ApiModelProperty(example = "Cain Cook")
    private String contactName;
}