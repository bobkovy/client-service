package ru.nnov.test.task.client.service.exceptions;

import java.util.UUID;

public class ApplicationNotFoundException extends RuntimeException {
    public ApplicationNotFoundException(UUID applicationId) {
        super("Application (id = " + applicationId + ") not found");
    }

    public ApplicationNotFoundException(String message) {
        super(message);
    }
}
