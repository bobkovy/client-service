package ru.nnov.test.task.client.service.entity;

import com.fasterxml.jackson.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "APPLICATION")
@EqualsAndHashCode()
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"contactId", "applicationId", "createdDate", "productName"})
@ApiModel
public class Application {
    @Id
    @JsonProperty("APPLICATION_ID")
    @Column(name = "APPLICATION_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(example = "9f3fa689-d81f-45ab-88ef-a616f0e3e6ee")
    private UUID id;
    @JsonProperty("DT_CREATED")
    @Column(name = "DT_CREATED", nullable = false, length = 36)
    @Builder.Default
    @ApiModelProperty(example = "2019-11-17T01:00:00")
    private LocalDateTime createdDate = LocalDateTime.now();
    @ApiModelProperty(example = "PRODUCT_1")
    @JsonProperty("PRODUCT_NAME")
    @Column(name = "PRODUCT_NAME", nullable = false, length = 36)
    private String productName;
    @JsonProperty("CONTACT_ID")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "CONTACT_ID")
    @JsonIdentityReference(alwaysAsId = true)
    @ManyToOne
    @JoinColumn(name = "CONTACT_ID", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @ApiModelProperty(value = "CONTACT_ID", example = "c54db8b0-63a0-4257-8aa2-2b7a8689e518", dataType = "java.lang.String")
    private Contact contact;
}
