package ru.nnov.test.task.client.service.exceptions;

import java.util.UUID;

public class ContactNotFoundException extends RuntimeException {
    public ContactNotFoundException(UUID contactId) {
        super("Contact (id = " + contactId + ") not found");
    }
}
