package ru.nnov.test.task.client.service.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nnov.test.task.client.service.entity.Contact;

import java.util.UUID;

@Repository
public interface ContactRepository extends CrudRepository<Contact, UUID> {
}
