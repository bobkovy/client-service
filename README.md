# Task

## What do we have?

Storage of customer contacts and their requests for banking products.
The contact has a unique identifier `CONTACT_ID` and an arbitrary number of
product requests.
Applications have the identifier `APPLICATION_ID`, the creation date `DT_CREATED` and the product name
`PRODUCT_NAME`.

## What needs to be done?
### Required
Write an application (REST service) that accepts the contact ID as input by the GET method. By the ID, you need to find and return the last (newest)
request of the contact in the response.
The response should be formatted as `JSON` with the fields `CONTACT_ID`, `APPLICATION_ID`, `DT_CREATED`,
`PRODUCT_NAME`.

Write a standalone application built in fat-JAR with all the necessary
libraries inside (Java, Spring Boot)

- [x] in-memory db with demo data (h2)

- [x] provide error handling – the database is unavailable, an incorrect request, etc.

- [x] write unit tests (JUnit + Spring Boot Tests with raising the application context)

- [x] building an application using Maven.

### Desirable

- [x] provide the service consumer with the opportunity to choose the response format – JSON / XML

- [x] handle exceptional situations (for example, a contact with the specified ID was not found)

- [x] prepare [service description using Swagger](doc/swagger.json) (code-first or contract-first is not essential).

---

# Задание

## Что у нас есть?

Хранилище клиентских контактов и их заявок на банковские продукты.
У контакта есть уникальный идентификатор `CONTACT_ID` и произвольное кол-во заявок на
продукты.
Заявки имеют идентификатор `APPLICATION_ID`, дату создания `DT_CREATED` и название продукта
`PRODUCT_NAME`.

## Что нужно сделать?
### Обязательно
Написать приложение (REST-сервис), принимающий методом GET на вход идентификатор
контакта. По идентификатору необходимо найти и вернуть в ответе последнюю (самую новую)
заявку контакта.
Ответ оформить в виде `JSON` с полями `CONTACT_ID`, `APPLICATION_ID`, `DT_CREATED`,
`PRODUCT_NAME`.

Написать standalone-приложение, собранное в fat-JAR со всеми необходимыми
библиотеками внутри (Java, Spring Boot)

- [x] in-memory db с демонстрационными данными (h2)

- [x] предусмотреть обработку ошибок – БД недоступна, некорректный запрос и пр.

- [x] написать unit-тесты (JUnit + Spring Boot Test’ы с поднятием контекста приложения)

- [x] сборка приложения с помощью Maven.

### Желательно

- [x] предоставить потребителю сервиса возможность выбора формата ответа – JSON / XML

- [x] обрабатывать исключительные ситуации (например, контакт с указанным идентификатором не найден)

- [x] подготовить [описание сервиса при помощи Swagger](doc/swagger.json) (code-first или contract-first – не принципиально). 
